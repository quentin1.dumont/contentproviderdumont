package com.example.contentproviderdumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Switch;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String asc = "ASC";
    private static final String desc = "DESC";

    private ArrayAdapter<String> adapter;

    private ListView listView;

    private Switch s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        s = findViewById(R.id.order_switch);
        listView = findViewById(R.id.list_view);
        ArrayList<String> words = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, words);
        listView.setAdapter(adapter);
        retrieveWords();
    }

    public void onClick(View view) {
        // retrieve the words from user dictionary
        if (view.getId() == R.id.add) {
            // add a new word to the user dictionary
            Intent intent = new Intent(this, AddWord.class);
            startActivityForResult(intent, 1);
        } else if (view.getId() == R.id.delete) {
            // delete a word from the user dictionary
            for (int i = 0; i < listView.getCount(); i++) {
                if (listView.isItemChecked(i)) {
                    getContentResolver().delete(
                            UserDictionary.Words.CONTENT_URI,
                            UserDictionary.Words.WORD + " = ?",
                            new String[]{adapter.getItem(i)}
                    );
                }
            }
            retrieveWords();
        } else if (view.getId() == R.id.order_switch) {
            // change the order of the words
            if (s.isChecked()) {
                s.setText(R.string.asc);
            } else {
                s.setText(R.string.desc);
            }
        }
    }

    public void retrieveWords() {
        Cursor cursor = null;
        listView.clearChoices();
        adapter.clear();
        try {
            cursor = getContentResolver().query(
                    UserDictionary.Words.CONTENT_URI,
                    new String[]{UserDictionary.Words.WORD},
                    null,
                    null,
                    UserDictionary.Words.WORD + " " + (s.isChecked() ? asc : desc)

            );
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        if (cursor != null) {

            while (cursor.moveToNext()) {
                String word = cursor.getString(0);
                adapter.add(word);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            retrieveWords();
        }
    }
}