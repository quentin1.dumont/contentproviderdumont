package com.example.contentproviderdumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddWord extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.add) {
            // add a new word to the user dictionary
            String word = ((EditText) findViewById(R.id.word)).getText().toString();
            if(!word.isEmpty()) {
                if (getContentResolver().query(
                        UserDictionary.Words.CONTENT_URI,
                        null,
                        UserDictionary.Words.WORD + " = ?",
                        new String[]{word},
                        null).getCount() == 0) {
                    ContentValues values = new ContentValues();
                    values.put(UserDictionary.Words.WORD, word);
                    values.put(UserDictionary.Words.LOCALE, R.string.lang);
                    getContentResolver().insert(
                            UserDictionary.Words.CONTENT_URI,
                            values
                    );
                    finish();
                } else {
                    Toast.makeText(this, R.string.aleady_exist, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, R.string.empty_word, Toast.LENGTH_SHORT).show();
            }
        }
    }
}